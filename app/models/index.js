const dbConfig = require("../config/db.config.js");
const mongoose = require("mongoose");
const model = require("./tutorial.model.js");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;

db.tutorials = model(mongoose);
module.exports = db;
