require("dotenv").config();

const express = require("express");
const cors = require("cors");
const db = require("./app/models");
const routes = require("./app/routes/tutorial.routes");
const app = express();

const PORT = process.env.PORT || 8080;

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connected to database!");
  })
  .catch((err) => {
    console.log("can not connected to database!", err);
    process.exit();
  });

let corsOption = {
  origin: "http://localhost:8081",
};

app.use(cors(corsOption));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {

  res.json({
    "message": "Home Route",
    "GET_get all tutorials": "api/tutorials",
    "GET_get tutorial by id": "api/tutorials/:id",
    "POST_add new tutorial": "api/tutorials",
    "PUT_Update tutorial by id": "api/tutorials/:id",
    "DELETE_delete tutorial by id": "api/tutorials/:id",
    "DELETE_Remove all tutorial": "api/tutorials",
    "GET_all published tutorial": "api/tutorials/published",
  });
});

routes(app);
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
